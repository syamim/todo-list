import { createWebHistory, createRouter } from "vue-router";
import Body from '../components/body';

// import Error503 from '../pages/error/error503';
/* Authentication */
import LoginOne from '../pages/authentication/login_one';

import RegisterImage from '../pages/authentication/register_image';
// todo
import Todo from "../pages/todo/index.vue"
const routes = [
    {
      path: '',
      redirect: '/app/todo'
    },
      {
        path: '/app',
        component: Body,
        children: [
        {
          path: 'todo',
          name: 'todo',
          component: Todo,
           meta: {
              title: 'Todo | Database Assignment',
            }
        },
        ]
      },
    {
      path:'/auth/login',
      name:'login',
      component: LoginOne,
      meta: {
          title: 'Login | Database Assignment',
        }
    },
    {
      path:'/auth/register',
      name:'register',
      component:RegisterImage,
      meta: {
          title: 'Register | Database Assignment',
        }
    },
]
const router = createRouter({
    history: createWebHistory(),
    routes,
  })
  router.beforeEach((to, from, next) => {
    if(to.meta.title)
      document.title = to.meta.title;
    const path = ['/auth/login','/auth/register'];
     if(path.includes(to.path) || localStorage.getItem('uid')){
      return next();
     }
     next('/auth/login');
});
  export default router