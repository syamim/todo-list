import axios from 'axios'
import router from '@/router'

// instance
const authRequest = axios.create({
  baseURL: process.env.VUE_APP_API_HOST + '/api/',
  timeout: 5000,
  headers: {'Authorization': 'Bearer ' + localStorage.getItem('uid')}
});

const deAuth = axios.create({
  baseURL: process.env.VUE_APP_API_HOST,
  timeout: 5000,
  headers: {'Authorization': 'Bearer ' + localStorage.getItem('uid')}
});

const plainRequest = axios.create({
  baseURL: process.env.VUE_APP_API_HOST + '/api/',
  timeout: 5000,
});

const authRequestInterceptor = (config) => {
  config.headers.Authorization = 'Bearer ' + localStorage.getItem('uid')
  return config
}


authRequest.interceptors.request.use(authRequestInterceptor)
authRequest.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  let name = '';
  switch (error.response.status) {
    case 401: name = 'login'; break;
  }
  router.push({name})
  return Promise.reject(error);
})
deAuth.interceptors.request.use(authRequestInterceptor)

export {authRequest, plainRequest, deAuth};