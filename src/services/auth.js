import { deAuth, plainRequest} from './index.js'

export async function auth_user (formData){
  let data
  await plainRequest.post('/auth/local', formData)
  .then(function(result){
    console.log("🚀 ~ file: auth.js:7 ~ .then ~ result:", result)
    localStorage.setItem('uid', result.data.jwt)
    localStorage.setItem('userData', JSON.stringify(result.data.user))
    data = {result: result.data, error: false}
  })
  .catch(function (error){
    console.log('error',error)
    data = {error: true, result: error.response.data}
  })
  return data
}

export async function register_user (formData) {
  console.log("🚀 ~ file: auth.js:20 ~ register_user ~ formData:", formData)
  let data 
  await plainRequest.post('/auth/local/register', formData)
  .then(function(result){
    localStorage.setItem('uid', result.data.jwt)
    localStorage.setItem('userData', JSON.stringify(result.data.user))
    data = {result, error: false}
  })
  .catch(function (error){
    console.log(error)
    data = {error: true, result: error.response.data}
  })

  return data
}

export async function delete_token () {
  let data = true
  localStorage.removeItem('uid')
  localStorage.removeItem('userData')
  return data
}